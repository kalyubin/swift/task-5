enum Color: String, CaseIterable {
    case clubs = "Clubs"
    case diamonds = "Diamonds"
    case hearts = "Hears"
    case spades = "Spades"
}

enum CardValue: Int, CaseIterable {
    case two = 2, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king, ace
}

class Card {
    let color: Color
    let cardValue: CardValue
    
    init(color: Color, cardValue: CardValue){
        self.color=color
        self.cardValue=cardValue
    }
    
    func getCard() -> String {
        return "\(color.rawValue) \(cardValue). "
    }
}

class Deck {
    var cards: [Card]
    
    init() {
        cards = [Card]()
        for color in Color.allCases {
            for cardValue in CardValue.allCases {
                let card = Card(color: color, cardValue: cardValue)
                cards.append(card)
            }
        }
    }
    
    func shuffle() {
        cards.shuffle()
    }

    func deal() -> [Card] {
        if(cards.count < 5){
            print("There are not enough cards in the deck")
            return []
        }
        else{
            var result = [Card]()
            for _ in 1...5{
                result.append(cards.removeFirst())
            }
 
            return result
        }
    }
}

class Hand {
    var cards: [Card]

    init(cards: [Card]){
        self.cards=cards
    }
    
    func cardValue() -> String {

        let flush = cards.allSatisfy({$0.color == cards[0].color})
    
        
        let straight = cards.enumerated().allSatisfy({$0 == 4 || $1.cardValue.rawValue == cards[$0 + 1].cardValue.rawValue + 1})
        
        if flush && straight {
            if cards[0].cardValue == .ace && cards[4].cardValue == .ten {
                return "Royal flush"
            } else {
                return "Straight flush"
            }
        }
        
        if let foak = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 4}) {
            return "Four of a Kind \(foak.cardValue)"
        }
        
        if let toak = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 3}),
           let pair = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 2 && card.cardValue != toak.cardValue}) {
            return "Full House \(toak.cardValue) and \(pair.cardValue)"
        }
        
        if flush {
            return "Flush \(cards.first!.color)"
        }
        
        if straight {
            return "Straight to \(cards.first!.cardValue)"
        }
        
        if let toak = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 3}) {
            return "Three of a Kind \(toak.cardValue)"
        }
        
        if let pair1 = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 2}),
           let pair2 = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 2 && card.cardValue != pair1.cardValue}) {
           
           
           return "Two Pairs of \(pair1.cardValue) and \(pair2.cardValue)"
        }
        
        if let pair = cards.first(where: {card in cards.filter({$0.cardValue == card.cardValue}).count == 2}) {
            return "One Pair of \(pair.cardValue)"
        }
        
        return "High Card \(cards[0].cardValue)"
    }
    
    func getCard() -> String {

        cards.sort(by: {$0.cardValue.rawValue > $1.cardValue.rawValue})
        return "\(cards.map({$0.getCard()}).joined(separator: " ")) Your combination: \(cardValue())"
    }
}

var deck = Deck()
deck.shuffle()

var hands: [Hand] = []

for _ in 1...5 {
    let hand = Hand(cards: deck.deal())
    hands.append(hand)
}

for hand in hands {
    print(hand.getCard())
}