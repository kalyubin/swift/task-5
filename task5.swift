class Vehicle {
    var name: String
    init(name: String) {
        self.name = name
    }
    func fuel() {
        print("...")
    }
}

class Car: Vehicle {
    override func fuel() {
        print("Petrol")
    }
}

class Truck: Vehicle {
    override func fuel() {
        print("Disel")
    }
}

let car = Car(name: "LadaGranta")
car.fuel()

let truck=Truck(name: "KAMAZ")
truck.fuel()

print()
/*------------------------------------------------*/

class House {
    let width, height: Int
    init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    
    func create() {
        print("Size of house: \(self.width * self.height)")
    }
    
    func destroy() {
        print("The house is destroyed")
    }
}

let myHouse = House(width: 235, height: 325)
myHouse.create()
myHouse.destroy()

print()
/*-------------------------------------------------*/

struct Student {
    let name: String
    let age: Int
    let averageScore: Double
}

class Sorting {
    var students: [Student]
    init(array: [Student]) {
        self.students = array
    }
    
    func nameSorting(asc: Bool) -> [Student] {
        if asc {
            self.students.sort(by: { $0.name.count < $1.name.count })
        }
        else {
            self.students.sort(by: { $0.name.count > $1.name.count })
        }
        return self.students
    }
    
    func ageSorting(asc: Bool) -> [Student]  {
        if asc {
            self.students.sort(by: { $0.age < $1.age })
        }
        else {
            self.students.sort(by: { $0.age > $1.age })
        }
        return self.students
    }
    
    func averageScoreSorting(asc: Bool) -> [Student]  {
        if asc {
            self.students.sort(by: { $0.averageScore > $1.averageScore })
        }
        else {
            self.students.sort(by: { $0.averageScore < $1.averageScore })
        }
        return self.students
    }
    
    
    class func gam(array: [Student]) {
        for (val, stud) in array.enumerated() {
            print("\(val+1): Name: \(stud.name) Age: \(stud.age) Average Score: \(stud.averageScore)")
        }
    }
}

let StudentList: [Student] = [
    Student(name: "Ivan", age: 20, averageScore: 3.3), 
    Student(name: "Vasiliy", age: 18, averageScore: 5.0),
    Student(name: "Sergey", age: 21, averageScore: 4.4), 
    Student(name: "Vasilisa", age: 22, averageScore: 2.8)
]

let sorter: Sorting = Sorting(array: StudentList)
print("Sort by name in ascending order:")
print(Sorting.gam(array: sorter.nameSorting(asc: true)))
print()
print("Sort by name in descending order:")
print(Sorting.gam(array: sorter.nameSorting(asc: false)))
print()
print("Sort by age in ascending order:")
print(Sorting.gam(array: sorter.ageSorting(asc: true)))
print()
print("sort by average score in descending order:")
print(Sorting.gam(array: sorter.averageScoreSorting(asc: false)))
